<?php

namespace Phr\Html\HtmlConstruct\HtmlConstructBase;

interface IHttpMethods 
{
    public const POST = 'POST';

    public const GET = 'GET';

    public const PUT = 'PUT';

    public const PATCH = 'PATCH';

    public const DELETE = 'DELETE';

}