<?php

namespace Phr\Html\HtmlConstruct\HtmlConstructBase;

interface IformChars
{
    public const DIV = '</div>';
}

class HtmlFrameBase implements IformChars
{   
    protected string  $divClass;

    public function __construct( string $_frame_div )
    {
        $this->divClass = $_frame_div;
    }
}