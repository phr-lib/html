<?php

namespace Phr\Html\HtmlConstruct\HtmlConstructBase;

interface HtmlFormChars 
{
    public const CLOSE_FORM = '</form>';
}

abstract class HtmlFormBase implements HtmlFormChars
{   
    protected static $method;

    protected static $action; 

    public function __construct( string $_method,  string $_action )
    {
        self::$method = $_method;

        self::$action = $_action;
    }
}