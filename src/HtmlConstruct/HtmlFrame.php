<?php

namespace Phr\Html\HtmlConstruct;

use Phr\Html\HtmlConstruct\HtmlConstructBase\HtmlFrameBase;

class HtmlFrame extends HtmlFrameBase
{   
    /**
     * 
     * @access public
     * @method display
     * Display frame content
     * 
     */
    public function display( string $_content ): void 
    {   
        echo '<div class="'. $this->divClass .'">'. $_content .'</div>';
    }

    /**
     * 
     * @access public
     * @method open
     * Opens a div tag
     * 
     */
    public function open( string|null $_frame_id = null ): void 
    {
        echo '<div class="'. $this->divClass .'" id="'. $_frame_id .'">';
    }

    public function close()
    {
        echo "</div>";
    }
}