<?php

namespace Phr\Html\HtmlConstruct;

use Phr\Html\HtmlConstruct\HtmlConstructBase\HtmlFormBase;

class HtmlForm extends HtmlFormBase
{   
    private static $inputVar = 'text';

    private static $details = 'details';

    private $inputClassFrame = 'inputFrame';

   
    /**
     * 
     * @access public 
     * @method start
     * Start form 
     * 
     */
    public function start( string $_formId ): void 
    {
        echo '<form id="'. $_formId .'" action="'. self::$action .'" method=" '. self::$method .'">';
    }

    /**
     * 
     * @method input
     * @var name
     * @var input_id
     * Displays input with default type of text. To change input type 
     * @see inputType
     * 
     */
    public function input( string $_name, string $_input_id, string|int|float $_value = '', string $_placeholder = '' ): void 
    {
        echo '<input 
        type="'.self::$inputVar.'" 
        name="'. $_name .'" 
        id="' . $_input_id  . '"
        class="' . self::$details  . '"
        value="' . $_value  . '" 
        placeholder="' . $_placeholder  . '" 
        />';
    }

    /**
     * @method submit
     * @var button_name
     */
    public function submit( string $_button_text )
    {
        echo '<button 
        type="submit" 
        value="'.$_button_text.'
        class="submitButton"
        ">'.$_button_text.'</button></form>';
    }

    /**
     * 
     * @method inputType
     * Sets new input type
     * 
     */
    public function inputType( string $_new_input_type ): void 
    {
        self::$inputVar = $_new_input_type;
    }

    public function inputLabel(string $label, string $_name, string $_input_id, string|int|float $_value = '', string $_placeholder = ''): void 
    {
        echo '<div class="'.$this->inputClassFrame.'">';
        echo '<div class="label">'.$label.'</div>';
        echo '<div class="input">';
        $this->input($_name, $_input_id, $_value, $_placeholder);
        echo "</div></div>";
    }
}