<?php

namespace Phr\Html\HtmlBase\HtmlBaseChars;

interface IHtmlChars 
{
    public const DOCUMENT = "<!DOCTYPE html>";

    public const HEAD = '<head>';

    public const HTML = '<html>';

    public const BODY = '<body>';

    public const HEAD_CLOSE = '</head>';

    public const BODY_END = '</body>';

    public const HTML_END = '</html>';
    
}