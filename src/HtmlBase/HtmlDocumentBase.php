<?php

namespace Phr\Html\HtmlBase;

use Phr\Html\HtmlBase\HtmlBaseChars\IHtmlChars;

abstract class HtmlDocumentBase implements IHtmlChars
{   
    /**
     * 
     * @access public
     * @method TITLE
     * Sets page title
     */
    public static function TITLE( string $_page_title ): void 
    {
        echo "<title>". $_page_title  ."</title>";
    }

    public function __construct( string $_page_title )
    {
        echo self::DOCUMENT;

        echo self::HTML;

        echo self::HEAD;

        self::TITLE( $_page_title );

        ?>
        <script src="https://source.ortus.si/js/jQuery.js"></script>
        <link rel="stylesheet" href="https://source.ortus.si/css/global_5_0_1.css">
        <?php

        echo self::HEAD_CLOSE;
    }

    public function endDocument()
    {
        echo self::BODY_END;
        echo self::HTML_END;
    }
    
}