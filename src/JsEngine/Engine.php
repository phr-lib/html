<?php

namespace Phr\Html\JsEngine;

class Engine 
{   
    private $appsettings;    

    public function __construct( string $_app_config )
    {   
        $this->appsettings = json_decode( file_get_contents( $_app_config ) );
    }

    public function serverSettings()
    {
        return json_encode( $this->appsettings );
    }
}